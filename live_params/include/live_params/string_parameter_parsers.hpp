#ifndef STRING_PARAMETER_PARSERS_HPP
#define STRING_PARAMETER_PARSERS_HPP

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <boost/tokenizer.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

namespace rpp
{
  inline std::string readFile(std::string filename)
  {
    std::ifstream ifs(filename.c_str(), std::ios::in | std::ios::binary | std::ios::ate);

    std::ifstream::pos_type fileSize = ifs.tellg();
    ifs.seekg(0, std::ios::beg);

    std::vector<char> bytes(fileSize);
    ifs.read(&bytes[0], fileSize);

    return std::string(&bytes[0], fileSize);
  }

  inline void writeFile(std::string filename, std::string data)
  {
    std::ofstream ofs(filename.c_str(), std::ofstream::out);
    ofs << data;
    ofs.close();
  }

  inline std::vector<std::string> parameterStringToStringVector(std::string string, boost::char_separator<char> sep = boost::char_separator<char>("[, ]"))
  {
    boost::tokenizer<boost::char_separator<char> > tokens(string, sep);
    std::vector<std::string> strings = std::vector<std::string>(tokens.begin(), tokens.end());
    return strings;
  }

  inline std::vector<std::pair<std::string, std::string> > parameterStringToStringPairVector(std::string string, boost::char_separator<char> sep = boost::char_separator<char>("[, ]"))
  {
    std::vector<std::string> pair_strings = parameterStringToStringVector(string, sep);
    boost::char_separator<char> inner_sep(":");
    std::vector<std::pair<std::string, std::string> > string_pairs;
    for(unsigned int i = 0; i < pair_strings.size(); i++)
    {
      boost::tokenizer<boost::char_separator<char> > tokens(pair_strings[i], inner_sep);
      std::vector<std::string> strings = std::vector<std::string>(tokens.begin(), tokens.end());
      if(strings.size() != 2)
      {
        std::cerr << "String pair parameter was the wrong size (" << strings.size();
        string_pairs.clear();
        return string_pairs;
      }
      string_pairs.push_back(std::pair<std::string, std::string>(strings[0], strings[1]));
    }
    return string_pairs;
  }

  inline std::vector<double> parameterStringToFPVector(std::string string, boost::char_separator<char> sep = boost::char_separator<char>("[, ]"))
  {
    std::vector<std::string> strings = parameterStringToStringVector(string, sep);

    std::vector<double> output;
    for(unsigned int i = 0; i < strings.size(); i++)
    {
      output.push_back(boost::lexical_cast<double>(strings[i]));
    }

    return output;
  }
}

#endif //STRING_PARAMETER_PARSERS_HPP
